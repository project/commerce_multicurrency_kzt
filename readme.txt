National Bank of Republic Kazakhstan as currency exchange rate sync providers 
for Commerce multicurrency module

DEPENDENCIES
Commerce Multicurrency provider for KZT depends on the Commerce multicurrency 
module.

INSTALLATION
Install the module as usual.

CONFIGURATION
1. Select "National Bank of Republic Kazakhstan" as sync provider on currency
   conversion settings page: admin/commerce/config/currency/conversion
2. Run cron or sync manually to synchronize the rates.



The module is created and maintained in Estonia by company Brilliant Solutions