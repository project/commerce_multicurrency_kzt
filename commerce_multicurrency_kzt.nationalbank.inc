<?php

/**
 * Fetch the currency exchange rates for the requested currency combination.
 * Use nationalbank.kz as provider.
 *
 * Return an array with the array(target_currency_code => rate) combination.
 *
 * @param string $currency_code
 *   Source currency code.
 * @param array $target_currencies
 *   Array with the target currency codes.
 *
 * @return array
 *   Array with the array(target_currency_code => rate) combination.
 */
function commerce_multicurrency_kzt_exchange_rate_sync_provider_nationalbank($currency_code, $target_currencies) {
  $data = cache_get(__FUNCTION__, 'cache');

  if (!$data) {
    $nationalbank_rates = array();
    if (($xml = @simplexml_load_file('http://www.nationalbank.kz/rss/rates_all.xml')) && @count($xml->channel->item)) {
      foreach ($xml->channel->item as $item) {
        $rate = (float) str_replace(',', '.', (string) $item->description);
        $nationalbank_rates[(string) $item->title] = empty($rate) ? 0 : $item->quant / $rate;
      }

      cache_set(__FUNCTION__, $nationalbank_rates, 'cache', time() + 3600);
    }
    else {
      watchdog(
        'commerce_multicurrency', 'Rate provider Nationalbank.kz: Unable to fetch / process the currency data of @url',
        array('@url' => 'http://www.nationalbank.kz/rss/rates_all.xml'),
        WATCHDOG_ERROR
      );
    }
  }
  else {
    $nationalbank_rates = $data->data;
  }

  $rates = array();
  foreach ($target_currencies as $target_currency_code) {
    if ($currency_code == 'KZT' && isset($nationalbank_rates[$target_currency_code])) {
      $rates[$target_currency_code] = $nationalbank_rates[$target_currency_code];
    }
    elseif (isset($nationalbank_rates[$currency_code]) && $target_currency_code == 'KZT') {
      $rates[$target_currency_code] = 1 / $nationalbank_rates[$currency_code];
    }
    elseif (isset($nationalbank_rates[$currency_code]) && isset($nationalbank_rates[$target_currency_code])) {
      $rates[$target_currency_code] = $nationalbank_rates[$target_currency_code] / $nationalbank_rates[$currency_code];
    }
  }

  return $rates;
}
